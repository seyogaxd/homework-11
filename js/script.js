function isNumberValid(value){
    return value!== null && value !== "" && !isNaN(value);
}

function isOperationValid(value){
    return value !== '/' && value !== '*' && value !=='-' && value !== '+';
}

let userInput;
let secondUserInput;
let userOperation;

do{
    userInput = prompt("Input first number:", userInput);
    secondUserInput = prompt("Input second number:", secondUserInput);
    userOperation = prompt("Input operation:", userOperation);
}while(!isNumberValid(userInput) || !isNumberValid(secondUserInput) || isOperationValid(userOperation));

function calc(firstNumber, secondNumber, operation){
    firstNumber = parseInt(firstNumber);
    secondNumber = parseInt(secondNumber);
    if(operation === '/') return firstNumber/secondNumber;
    else if(operation === '*') return firstNumber*secondNumber;
    else if(operation === '+') return firstNumber+secondNumber;
    else if(operation === '-') return firstNumber-secondNumber;
}
console.log(calc(userInput, secondUserInput, userOperation));